#include "FFT.h"
#include <cstdio>
#include <cmath>

/* Use Q15 fixed point format (1 sign bit plus 15 fractional bits) */
#define FIXED_POINT 15
#define WORD_SIZE   16

#define WORDS_PER_LINE 4
#define WORDS_PER_GROUP 1

mem input_a_re, input_a_im, input_b_re, input_b_im, output_a_re, output_a_im, output_b_re, output_b_im, twiddle_re, twiddle_im; 

int to_fixed(float n)
{
	int res = (int)(n * (1 << FIXED_POINT));
	/* Crop results */
	if (res > (1 << WORD_SIZE - 1) - 1)
		return (1 << WORD_SIZE - 1) - 1;
	if (res < -(1 << WORD_SIZE - 1))
		return -(1 << WORD_SIZE - 1);
	return res;
}

float from_fixed(int n)
{
	return n / (float)(1<<FIXED_POINT);
}

void print_mem(mem m, int offset, int size, bool fixed, bool newline)
{
	int i;
	for(i = offset;i<offset+size;i++)
	{
		if (fixed)
			printf("%0.4f", from_fixed(get_mem(m->id, i)));
		else
			printf("%04hx", (short)get_mem(m->id, i));
		if (newline && (i + 1) % WORDS_PER_LINE == 0)
			printf("\n");
		else if ((i + 1) % WORDS_PER_GROUP == 0)
			printf(" ");
	}
	if (newline && i % WORDS_PER_LINE != 0)
		printf("\n");
}


void pre_run()
{
	int i;
	/* Assign memories, at least for the first stage */
	input_a_re   = alloc_mem(P0M0);
	input_a_im   = alloc_mem(P1M0);
	input_b_re   = alloc_mem(P2M0);
	input_b_im   = alloc_mem(P3M0);
	
	twiddle_re   = alloc_mem(P4M0);
	twiddle_im   = alloc_mem(P4M1);
	
	/* TODO: Init memory and twiddles */
	for (i=0;i<PARAM_N_t/2;i++)
	{
		set_mem(twiddle_re->id, i, to_fixed(cos(i*2*M_PI/PARAM_N_t)));
		set_mem(twiddle_im->id, i, to_fixed(sin(i*2*M_PI/PARAM_N_t)));
	}
	
	for (i=0;i<PARAM_N_t;i++)
	{
		/* We take the sine from 0 to 20*2*Pi, ie twenty periods. We divide 
		 * the value by PARAM_N_t to prevent overflow. */
		int value = to_fixed(sin((float)i*20*2*M_PI/PARAM_N_t)/PARAM_N_t);

		if (i<PARAM_N_t/2)
		{
			if (i % 2 == 0) {
				set_mem(input_a_re->id, i, value);
				set_mem(input_a_im->id, i, 0);
			} else {
				set_mem(input_b_re->id, i, value);
				set_mem(input_b_im->id, i, 0);
			}
		}
		else
		{
			if (i % 2 == 0) {
				set_mem(input_b_re->id, i - PARAM_N_t/2, value);
				set_mem(input_b_im->id, i - PARAM_N_t/2, 0);
			} else {
				set_mem(input_a_re->id, i - PARAM_N_t/2, value);
				set_mem(input_a_im->id, i - PARAM_N_t/2, 0);
			}
		}
	}
	
	printf("re(W)\n");
	print_mem(twiddle_re, 0, PARAM_N_t/2, true, true);
	printf("im(W)\n");
	print_mem(twiddle_im, 0, PARAM_N_t/2, true, true);
	printf("re(in_a)\n");
	print_mem(input_a_re, 0, PARAM_N_t/2, true, true);
	printf("re(in_b)\n");
	print_mem(input_b_re, 0, PARAM_N_t/2, true, true);

	printf("re_in = [");
	print_mem(input_a_re, 0, PARAM_N_t/2, true, false);
	print_mem(input_b_re, 0, PARAM_N_t/2, true, false);
	printf("];\n");

/* Write out memory contents for use by the python simulator */
	save_mem_range_to_file(input_a_re->id, 0, PARAM_N_t/2, "Memory/sin_a_re.mm");
	save_mem_range_to_file(input_a_im->id, 0, PARAM_N_t/2, "Memory/sin_a_im.mm");
	save_mem_range_to_file(input_b_re->id, 0, PARAM_N_t/2, "Memory/sin_b_re.mm");
	save_mem_range_to_file(input_b_im->id, 0, PARAM_N_t/2, "Memory/sin_b_im.mm");
	save_mem_range_to_file(twiddle_re->id, 0, PARAM_N_t/2, "Memory/twiddle_re.mm");
	save_mem_range_to_file(twiddle_im->id, 0, PARAM_N_t/2, "Memory/twiddle_im.mm");
}

void post_run()
{
	if (PARAM_n_t % 2 == 0) {
		/* When the number of stages is even, the 
		 * outputs end up at the left memories again */
		output_a_re  = alloc_mem(P0M0);
		output_a_im  = alloc_mem(P1M0);
		output_b_re  = alloc_mem(P2M0);
		output_b_im  = alloc_mem(P3M0);
	} else {
		output_a_re  = alloc_mem(P0M1);
		output_a_im  = alloc_mem(P1M1);
		output_b_re  = alloc_mem(P2M1);
		output_b_im  = alloc_mem(P3M1);
	}
	printf("re_out = [");
	print_mem(output_a_re, 0, PARAM_N_t/2, true, false);
	print_mem(output_b_re, 0, PARAM_N_t/2, true, false);
	printf("];\n");
	printf("im_out = [");
	print_mem(output_a_im, 0, PARAM_N_t/2, true, false);
	print_mem(output_b_im, 0, PARAM_N_t/2, true, false);
	printf("];\n");
	/*
	printf("re(out)\n");
	print_mem(output_a_re, 0, PARAM_N_t/2, false, true);
	print_mem(output_b_re, 0, PARAM_N_t/2, false, true);
	printf("im(out)\n");
	print_mem(output_a_im, 0, PARAM_N_t/2, false, true);
	print_mem(output_b_im, 0, PARAM_N_t/2, false, true);
	*/

}
