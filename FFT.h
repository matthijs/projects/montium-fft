#include "libmontiumc.h"
#ifndef FFT_H_INCLUDED
#define FFT_H_INCLUDED

/* Define some parameters for this FFT algorithm. We prefix them 
 * with PARAM_ so we can still use variable names like 'n' :-) */
/* Change these: */
/* 2log of number of tiles */
#define PARAM_q 2
/** 2log of total FFT size */
#define PARAM_n 12

/* Note that the FFT size on each tile 2^(n-q) must be at least
 * 8 and always a multiple of 4. The number of stages on each 
 * tile (n-q) must also be a multiple of 2. */

/* But don't change these: */
/* Number of tiles */
#define PARAM_Q (1 << PARAM_q)
/** Total FFT size */
#define PARAM_N (1 << PARAM_n)
/** FFT size on each tile */
#define PARAM_N_t (PARAM_N / PARAM_Q)
/** 2log of FFT size on each tile */
#define PARAM_n_t (PARAM_n - PARAM_q)

#ifndef __MONTIUMCC__
	void pre_run();
	void post_run();
#endif	
	/**
	 * Support structure to store the result of a butterfly.
	 */
	struct bf_out {
		word a_re;
		word a_im;
		word b_re;
		word b_im;
	};
	
	/**
	 * Support structure to store teh inputs for a butterfly.
	 */
	struct bf_in {
			word a_re;
			word a_im;
			word b_re;
			word b_im;
			word W_re;
			word W_im;
	};
	
	/**
	 * A struct to hold all the used memories. We put these in
	 * a struct, so we can store them in a local variable and 
	 * pass them around in arguments, so we can change the memories
	 * allocated to each on ever stage (MontiumC doesn't support
	 * reassigning global mem variables).
	 */
	struct mems {
		mem input_a_re, input_a_im, input_b_re, input_b_im, output_a_re, output_a_im, output_b_re, output_b_im, twiddle_re, twiddle_im;
	};
	
	INLINE struct bf_out butterfly(struct bf_in in);
	void run(void);	
	
	/* Values for the second_half argument */
#define FIRST_HALF 0
#define SECOND_HALF 1
	
	/* Values for the stage_odd argument */
#define EVEN_STAGE 0
#define ODD_STAGE 1
	
	/* Values for the cycle_odd argument */
#define EVEN_CYCLE 0
#define ODD_CYCLE 1
	
enum in_strategy {	
	REGULAR_IN,
	DISTRIBUTED_IN,
};

enum out_strategy {
	REGULAR_OUT,
	DISTRIBUTED_OUT,
	BITREVERSED_OUT,
};

#endif // !FFT_H_INCLUDED
