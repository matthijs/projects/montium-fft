import os
import sys

base_path = os.path.split(sys.argv[0])[0]

binary_name = os.path.join("Montium", "FFT.mb")
simulate(os.path.join(base_path, binary_name))

loadMMFile('p0m0', os.path.join(base_path, 'Memory', 'sin_a_re.mm'))
loadMMFile('p1m0', os.path.join(base_path, 'Memory', 'sin_a_im.mm'))
loadMMFile('p2m0', os.path.join(base_path, 'Memory', 'sin_b_re.mm'))
loadMMFile('p3m0', os.path.join(base_path, 'Memory', 'sin_b_im.mm'))
loadMMFile('p4m0', os.path.join(base_path, 'Memory', 'twiddle_re.mm'))
loadMMFile('p4m1', os.path.join(base_path, 'Memory', 'twiddle_im.mm'))

addBreakpoint("gpo == 0b000001")

runUntilNotHold()
